package bubbles

import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLBodyElement
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Date
import kotlin.js.Math

external class Object

val width = window.innerWidth
val height = window.innerHeight
var bubbleArray = emptyArray<Object>()
var drawBubbles: () -> Unit = {}
var start: () -> Unit = {}
var colorInterpolation = js("d3.interpolateRgb('red', 'blue')") as (Double) -> String

fun onLoad() {
    val colorChangeDurationInMilliseconds = 4000
    val bubbleRadius = 20
    val numberOfBubbles = (width / bubbleRadius) * (height / bubbleRadius) / 5

    val body = document.body as HTMLBodyElement

    val canvas = document.createElement("canvas") as HTMLCanvasElement
    canvas.width = width
    canvas.height = height
    body.appendChild(canvas)

    val context = canvas.getContext("2d") as CanvasRenderingContext2D
    context.lineWidth = 2.0

    drawBubbles = {
        context.clearRect(0.0, 0.0, width.toDouble(), height.toDouble())

        val t = (Date().getTime() % colorChangeDurationInMilliseconds) / colorChangeDurationInMilliseconds
        if (t > 0.5) {
            context.strokeStyle = colorInterpolation(2 * (1 - t))
        } else {
            context.strokeStyle = colorInterpolation(2 * t)
        }

        bubbleArray.forEach {
            val circle = it.asDynamic()
            context.beginPath()
            context.arc(circle.x as Double, circle.y as Double, circle.radius as Double, 0.0, 2 * Math.PI)
            context.stroke()
        }
    }

    start = {
        bubbleArray = emptyArray()

        for (i in 0..numberOfBubbles) {
            val circle = Object()
            circle.asDynamic().index = i
            circle.asDynamic().x = bubbleRadius + Math.random() * (width - 2 * bubbleRadius)
            circle.asDynamic().y = bubbleRadius + Math.random() * (height - 2 * bubbleRadius)
            circle.asDynamic().radius = bubbleRadius / 4 + Math.random() * (3 * bubbleRadius / 4)
            bubbleArray += circle
        }

        js("""
d3.forceSimulation(d3_bubbles_main.bubbles.bubbleArray)
  .velocityDecay(0.95)
  .force('collision', d3.forceCollide().radius(function(d) {
    return d.radius
  }))
  .on('tick', d3_bubbles_main.bubbles.drawBubbles)
  .on('end', d3_bubbles_main.bubbles.start)
        """) as Unit
    }

    start()
}

fun main(args: Array<String>) {
    document.addEventListener("DOMContentLoaded", { onLoad() })
}